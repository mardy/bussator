# -*- encoding: utf-8 -*-

from __future__ import unicode_literals

from bussator import make_app

application = make_app('/config/config.ini')
