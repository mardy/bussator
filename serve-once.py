#!/usr/bin/env python
# -*- coding: utf-8 -*-

from bussator import make_app
from wsgiref.simple_server import make_server

app = make_app('bussator.ini')
httpd = make_server('', 8000, app)
print("Serving HTTP on port 8000...")

# Serve one request, then exit
httpd.handle_request()
