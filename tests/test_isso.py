#!/usr/bin/env python
# -*- coding: utf-8 -*-

import pytest
import requests_mock

import bussator.globals
from bussator.plugins.isso import IssoPublisher


class PostData:
    def __init__(self, **params):
        self.author_name = None
        self.author_email = None
        self.author_photo = None
        self.author_url = None
        self.post_url = None
        self.post_title = ''
        self.post_date = None
        self.post_content_text = ''
        self.post_content_html = ''
        self.is_like = False
        for k, v in params.items():
            setattr(self, k, v)


@pytest.mark.parametrize('config,expected_headers,expected_url', [
    pytest.param({
                     'server_url': 'http://www.mardy.it/webmentions',
                 },
                 {
                     'User-Agent': bussator.globals.user_agent,
                 },
                 'http://www.mardy.it/webmentions/new',
                 id='No user agent'),
    pytest.param({
                     'server_url': 'http://localhost:3000/webmentions',
                     'user_agent': 'Super Test/3.0',
                 },
                 {
                     'User-Agent': 'Super Test/3.0',
                 },
                 'http://localhost:3000/webmentions/new',
                 id='with UA'),
    ])
def test_init(config, expected_headers, expected_url):
    plugin = IssoPublisher(config)
    assert plugin.headers == expected_headers
    assert plugin.publish_uri == expected_url


@pytest.mark.parametrize('config,source,target,post_data,'
                         'expected_query,expected_body', [
    pytest.param({
                     'server_url': 'http://www.mardy.it/webmentions',
                 },
                 'http://www.mardy.it/source-post/',
                 'http://example.org/target-post',
                 PostData(),
                 'uri=%2ftarget-post',
                 {
                     'author': None,
                     'email': None,
                     'text': 'I wrote a reply [in my blog]'
                             '(http://www.mardy.it/source-post/)',
                     'title': '',
                     'website': None,
                 },
                 id='empty data'),
    pytest.param({
                     'server_url': 'http://www.mardy.it/webmentions',
                     'like_message': 'Thumbs up {post_url}',
                 },
                 'http://www.mardy.it/post/',
                 'http://example.org/target-post',
                 PostData(is_like=True),
                 'uri=%2ftarget-post',
                 {
                     'author': None,
                     'email': None,
                     'text': 'Thumbs up http://www.mardy.it/post/',
                     'title': '',
                     'website': None,
                 },
                 id='like'),
    pytest.param({
                     'server_url': 'http://www.mardy.it/webmentions',
                 },
                 'http://www.mardy.it/post/',
                 'http://example.org/target-post',
                 PostData(post_content_html='Indeed!'),
                 'uri=%2ftarget-post',
                 {
                     'author': None,
                     'email': None,
                     'text': 'Indeed!\n\n'
                             '[Source link](http://www.mardy.it/post/)',
                     'title': '',
                     'website': None,
                 },
                 id='content'),
    ])
def test_process(config, source, target, post_data,
                 expected_query, expected_body):
    plugin = IssoPublisher(config)

    with requests_mock.Mocker() as mock:
        m = mock.post('http://www.mardy.it/webmentions/new')
        plugin.process_webmention(source, target, post_data)

    assert m.called_once
    req = m.last_request

    assert req.query == expected_query
    assert req.json() == expected_body
